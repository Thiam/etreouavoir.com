<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesManager@accueil');
Route::get('/accueil', 'PagesManager@accueil');
Route::get('/blog', 'PagesManager@blog');
Route::get('/blog/{slug}', 'PagesManager@article');
Route::get('/galerie', 'PagesManager@galerie');
Route::get('/a-propos', 'PagesManager@apropos');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
