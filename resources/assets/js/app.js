
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

window.Vue = require('vue');
window.Materialize = require('materialize-css');
window.axios = require('axios');
const swal = require('sweetalert2');
window.firstLoad = true;

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

window.vue_data = {
    page_title : entry_page_title,
    page_content : entry_page_content,
    articles: [],
    categories: [],
    nombre_articles: 0,
    aff_categ: -1,
    photos: [],
    nombre_photos: 0,
    pagination_pos: 1,
    loading: true,
    windowWidth: 0,
    windowHeight: 0
};

window.vue_set = {
    el: '#app',
    data: vue_data,
    methods: {
        gotoPage: function(target) {
            getPage(target);
        },
        getWindowWidth(event) {
            this.windowWidth = document.documentElement.clientWidth;
        },

        getWindowHeight(event) {
            this.windowHeight = document.documentElement.clientHeight;
        }
    },
    created: function() {
        axios.get('/api/blog/articles/json').then(response => {
            this.nombre_articles = response.data.nombre;
            this.articles = response.data.articles;
            this.categories = response.data.categories;
            $('#loading-bar').hide();
        }).catch((error) => {
            swal(
                "Oups!",
                "Une erreur est survenue lors du chargement des articles: "+error,
                "error"
            );
            $('#loading-bar').hide();
        });
        axios.get('/api/photos/json').then(response => {
            this.nombre_photos = response.data.nombre;
            this.photos = response.data.photos;
            $('#loading-bar').hide();
        }).catch((error) => {
            swal(
                "Oups!",
                "Une erreur est survenue lors du chargement des photos: "+error,
                "error"
            );
            $('#loading-bar').hide();
        });
    },
    mounted: function() {
        this.$nextTick(function() {
            window.addEventListener('resize', this.getWindowWidth);
            window.addEventListener('resize', this.getWindowHeight);

            //Init
            this.getWindowWidth();
            this.getWindowHeight();

            $(".btn-sidenav").sideNav({
              closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
              draggable: true, // Choose whether you can drag to open on touch screens,
            });
            $('.materialboxed').materialbox();
            $('.parallax').parallax();
            $('.parallax').parallax();
            if(!window.firstLoad)
                FB.XFBML.parse();
        })
    },
    beforeDestroy() {
        window.removeEventListener('resize', this.getWindowWidth);
        window.removeEventListener('resize', this.getWindowHeight);
    }
};

let app = new Vue(vue_set);

/** Mon code #Medteck **/
$(document).ready(function(){
    //$('.materialboxed').materialbox();
    setTimeout(function() {
        $(".btn-sidenav").sideNav({
            closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
            draggable: true, // Choose whether you can drag to open on touch screens,
        });
        $('.materialboxed').materialbox();
        $('.parallax').parallax();
        $('.parallax').parallax();
        window.firstLoad = false;
    } ,1200);
});
let nothing = function() {/*Nothing*/};
// Is Promise #ES6 supported ?
let promiseSupported = function() {
    return (typeof Promise !== "undefined" && Promise.toString().indexOf("[native code]") !== -1);
};
// Get a page fragment
window.getPage = (href) => {
    $('#loading-bar').show();
    if(!href) return;
    if(promiseSupported())
    {
        axios.get('/'+href).then(response => {
            let parser = new DOMParser();
            let parsedHTML = parser.parseFromString(response.data,"text/html");
            document.querySelector('#page_title').innerHTML = entry_page_title;
            $('#app-page-content').fadeOut("slow", function(){
                $(this).replaceWith(parsedHTML.querySelector('#app-page-content'));
                //app.$destroy();
                app = new Vue(vue_set);
                //app.$forceUpdate();
                //app.compile();
                $('#app-page-content').fadeIn("fast");
                $('#loading-bar').hide();
            });
            let title = parsedHTML.querySelector("title").innerHTML;
            window.history.pushState(title, title, response.config.url);
            document.title = title;
            $("html, body").animate({ scrollTop: 0 }, "slow");
        }).catch(error => {
            console.log(error);
            swal(
                "Désolé!",
                "Une erreur est survenue lors du chargement de votre page. Vous allez être redirigé.",
                "warning"
            ).then(function() {
                window.location.href = href;
            });
        });
    }
    else
    {
        // Sur les navigateur médiévaux comme Explorer -_-
        window.location.href = href;
    }
}
