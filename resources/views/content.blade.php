@extends('welcome')

@section('app')


    <div class="col s12 m12 l12 blue-grey darken-2 center-align z-depth-4 mobile-only mobile-head">
        <div class="white-text col l12 m12 s12">
            <a href="#!user"><img class="circle medium" id="img-mamadou-mobile" src="/img/etreouavoir-blog-moustapha-diallo.jpg"></a>
            <h3 class="light">Être ou Avoir</h3>
            <p class="flow-text medium">
                La voie de l'humanité <br>
                Mankind's way
            </p>
        </div>
    </div>

    <div class="col s12 m12 l12 no-padding">

        <div class="col l12 m12 s12 no-padding" id="page_title_container">
            <div class="col l6 offset-l1 m6 offset-m1 s10 offset-s1 no-padding">
                <h1 class="blue-grey-text" id="page_title">{{$page_title}}</h1>
                <hr>
            </div>
        </div>

        <div id="app-page-content" class="app-actual-content">
            @include('pages.'.$page)
        </div>

    </div>


@endsection
