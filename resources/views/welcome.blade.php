<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Être ou Avoir - Le Blog | {{$page_title}}</title>


        <meta property="fb:app_id" content="161419814608300">
        <meta name="url" content="{{$seo_url}}">
        <meta property="og:url" content="{{$seo_url}}">
        <meta name="description" content="{{$seo_description}}">
        <meta property="og:description" content="{{$seo_description}}">
        <meta name="type" content="{{$seo_type}}">
        <meta property="og:type" content="{{$seo_type}}">
        <meta name="title" content="{{$seo_title}}">
        <meta property="og:title" content="{{$seo_title}}">
        <meta name="keywords" content="{{$seo_keywords}}">
        <meta name="image" content="{{$seo_image}}">
        <meta property="og:image" content="{{$seo_image}}">
        <meta property="og:image:alt" content="{{url('/img/mamadou-diallo.jpg')}}">

        <!-- Fonts -->
        <link rel="stylesheet" href="/css/app.css">
        <link rel="stylesheet" href="/css/materialize.css">
        <script type="text/javascript">
            window.entry_page_title = "{{$page_title}}";
            window.entry_page_content = "accueil";
        </script>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-110695584-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-110695584-1');
        </script>


    </head>
    <body>


        <div>

            <div class="progress" style="position:absolute;" id="loading-bar">
                <div class="indeterminate"></div>
            </div>

            <a class="btn-floating btn-large blue-grey btn-sidenav" data-activates="slide-out">
                  <i class="material-icons">menu</i>
            </a>
            <ul id="slide-out" class="side-nav fixed blue-grey darken-2 center-align valign-wrapper z-depth-4">
                <div class="row">
                    <div class="white-text col l12 m12 s12 sidenav-titles">
                        <a href="/accueil"><img class="circle medium" src="{{url('/img/etreouavoir-blog-moustapha-diallo.jpg')}}"></a>
                        <h2 class="light">Être ou Avoir</h2>
                        <p class="flow-text medium">
                            La voie de l'humanité <br>
                            Mankind's Way
                        </p>
                    </div>
                    <div class="links-list">
                        <ul>
                            <li>
                                <a class="blue-grey lighten-2 waves-effect waves-light hoverable btn-large" onclick="entry_page_title='Accueil'; getPage('accueil')"> Accueil </a>
                            </li>
                            <li>
                                <a class="blue-grey lighten-2 waves-effect waves-light hoverable btn-large" onclick="entry_page_title='Blog'; getPage('blog')"> Blog </a>
                            </li>
                            <li>
                                <a class="blue-grey lighten-2 waves-effect waves-light hoverable btn-large" onclick="entry_page_title='Galerie'; getPage('galerie')"> Galerie </a>
                            </li>
                            <li>
                                <a class="blue-grey lighten-2 waves-effect waves-light hoverable btn-large" onclick="entry_page_title='À propos'; getPage('a-propos')"> À propos </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col l12 m12 s12">
                        <hr>
                        <p>
                            <a class="white-text" target="_blank" href="{{url('https://medteck.xyz')}}">2017, Développé par Medteck <i class="material-icons tiny">open_in_new</i> </a>
                        </p>
                    </div>
                </div>
            </ul>

            <div class="row" id="app">

                <!-- yield the actual content :D -->
                @yield('app')
                <!-- End of the content -->

            </div>
        </div>

        <script type="text/javascript" src="{{url('/js/app.js')}}"></script>

        <!-- Facebook -->
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = 'https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.11&appId=161419814608300';
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.11&appId=161419814608300';
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    </body>
</html>
