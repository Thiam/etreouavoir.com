<div class="row">
    <div class="col l10 offset-l1 m10 offset-m1 s12">

        <h3>{{$contenu->excerpt}}</h3>

        <div class="article-body">
            {!!$contenu->body!!}
        </div>

    </div>

</div>
