<div class="col l10 offset-l1 m10 offset-m1 s10 offset-s1 no-padding">
    <blockquote>
        <p class="flow-text">
            Nous n'héritons pas de la terre de nos ancêtres, nous l'empruntons à nos enfants.
        </p>
        <p class="flow-text">
            We do not inherit the land of our ancestors, we borrow it from our children.
        </p>
    </blockquote>
    <br>
    <h3 class="blue-grey-text text-darken-2">Dernières publications</h3>
    <div class="col l12 m12 s12 no-padding">

        <div class="col s12 m12 l4" v-for="art in articles.slice(0,3)" onclick="entry_page_title='Blog';" v-on:click="gotoPage('blog/'+art.slug)">
            <div class="card hoverable waves-effect waves-block waves-light">
                <div class="card-image">
                    <img v-bind:src="art.image">
                </div>
                <div class="card-content">
                    <span class="card-title truncate">@{{art.title}}</span>
                    <p class="truncate">
                        @{{art.desc}}
                    </p>
                </div>
                <div class="card-action">
                    <a href="#" class="blue-grey-text bold">Lire cet article..</a>
                </div>
            </div>
            <br> <br>
        </div>
    </div>

    <h3 class="blue-grey-text text-darken-2">Dernières images</h3>
    <div class="col l12 m12 s12">

        <div class="col s12 m12 l6" v-for="art in photos.slice(0,2)">
            <img class="materialboxed img-prev hoverable" v-bind:src="art.image">
            <h4 class="center-align">@{{art.title}}</h4>
        </div>

    </div>
</div>
