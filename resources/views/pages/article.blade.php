@php(Carbon\Carbon::setLocale('fr'))
<div class="row">

    <div class="banner-article parallax-container">
        <div class="parallax">
            <img src="{{$article->image_url()}}" class="img-parallax" />
        </div>
    </div>

    <div class="col l10 offset-l1 m10 offset-m1 s12">

        <div class="col m12">
            <h4 class="grey-blue-text text-darken-2">{{$article->title}}</h4>
            <p class="bold">
                Publié {{Carbon\Carbon::parse($article->created_at)->diffForHumans()}}
            </p>
        </div>

        <div class="col l12 m12 s12">
            <div class="fb-like" data-href="{{$seo_url}}" data-layout="button" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
            <br> <br>
        </div>

        <div class="col l12 m12 s12">
            <p>
                <h5 class="grey-blue-text text-darken-2">{{$article->excerpt}}</h5>
                <div class="article-body">
                    {!!$article->body!!}
                </div>
            </p>
        </div>

        <div class="col l12 m12 s12">
            <br>
            <h4>Commentaires</h4>
            <hr>

            <div class="col l12 m12 s12">
                <div class="fb-comments" data-width="100%" data-href="{{$seo_url}}" data-numposts="5"></div>
            </div>
        </div>
    </div>

</div>
