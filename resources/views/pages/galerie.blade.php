<div class="row">

    <div class="col l10 offset-l1 m10 offset-m1 s12">

        <div class="col l12 m12 s12 center-align" v-show="nombre_photos>4">
            <ul class="pagination">
                <li v-bind:class="pagination_pos > 1 ? 'waves-effect' : 'disabled'" v-on:click="pagination_pos > 1 ? pagination_pos-- : nothing()"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                <li v-for="j in parseInt(nombre_photos/16+(nombre_photos%16!=0))" v-bind:class="j==pagination_pos ? 'blue-grey lighten-2' : ''" v-on:click="pagination_pos=j"><a href="#!">@{{j}}</a></li>
                <li v-bind:class="pagination_pos < nombre_photos/16 ? 'waves-effect' : 'disabled'" v-on:click="pagination_pos < nombre_photos/16 ? pagination_pos++ : nothing()"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
            </ul>
        </div>

        <div class="col s12 m12 l6">

            <div class="col s12 m12 l12" v-for="art of photos" v-if="art.num%2!=0 || windowWidth >= 993" v-show="art.num>pagination_pos*16-16 && art.num<=pagination_pos*16" v-bind:key="art.id" >

                <img v-bind:src="art.image" alt="art.title" v-bind:data-caption="art.title" class="materialboxed img-prev" />

            </div>

        </div>

        <div class="col s12 m12 l6">

            <div class="col s12 m12 l12" v-for="art of photos" v-if="art.num%2==0 && art.num%2==0" v-show="art.num>pagination_pos*16-16 && art.num<=pagination_pos*16" v-bind:key="art.id" >

                <img v-bind:src="art.image" alt="art.title" v-bind:data-caption="art.title" class="materialboxed img-prev" />
                <br> <br>
            </div>

        </div>



        <h3 class="center-align" v-if="nombre_photos==0">Il n'y a aucune photo à afficher</h3>

        <div class="col l12 m12 s12 center-align" v-show="nombre_photos>10">
            <ul class="pagination">
                <li v-bind:class="pagination_pos > 1 ? 'waves-effect' : 'disabled'" v-on:click="pagination_pos > 1 ? pagination_pos-- : nothing()"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                <li v-for="j in parseInt(nombre_photos/16+(nombre_photos%16!=0))" v-bind:class="j==pagination_pos ? 'blue-grey lighten-2' : ''" v-on:click="pagination_pos=j"><a href="#!">@{{j}}</a></li>
                <li v-bind:class="pagination_pos < nombre_photos/16 ? 'waves-effect' : 'disabled'" v-on:click="pagination_pos < nombre_photos/16 ? pagination_pos++ : nothing()"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
            </ul>
        </div>


    </div>

</div>
