<div class="row">

    <div class="col l10 offset-l1 m10 offset-m1 s12">

        <nav class="nav-extended">
            <div class="nav-content darken-4">
                <ul class="tabs tabs-transparent">
                    <li class="tab center-align active" v-on:click="aff_categ=-1;">
                        <a>Toutes les rubriques</a>
                    </li>
                    <li v-for="categorie of categories" class="tab center-align" v-on:click="aff_categ=categorie.id;">
                        <a>@{{categorie.name}}</a>
                    </li>
                </ul>
            </div>
        </nav>
        <br><br>

        <div class="col s12 m12 l6 no-padding">
            <div class="col s12 m12 l12" v-for="art of articles" v-if="art.num%2!=0 || (art.num%2==0 && windowWidth <= 993)" v-show="art.num>pagination_pos*16-16 && art.num<=pagination_pos*16 && (aff_categ==-1 || aff_categ==art.categ)" v-bind:key="art.id" v-on:click="gotoPage('blog/'+art.slug)" >
                <div class="card hoverable waves-effect waves-block waves-light">
                    <div class="card-image">
                        <img v-bind:src="art.image">
                    </div>
                    <div class="card-content">
                        <span class="card-title truncate">@{{art.title}}</span>
                        <p class="truncate">
                            @{{art.desc}}
                        </p>
                    </div>
                    <div class="card-action">
                        <a href="#" class="blue-grey-text bold">Lire cet article..</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col s12 m12 l6 no-padding">
            <div class="col s12 m12 l12" v-for="art of articles" v-if="windowWidth> 993 && art.num%2==0" v-show="art.num>pagination_pos*16-16 && art.num<=pagination_pos*16 && (aff_categ==-1 || aff_categ==art.categ)" v-bind:key="art.id" v-on:click="gotoPage('blog/'+art.slug)" >
                <div class="card hoverable waves-effect waves-block waves-light">
                    <div class="card-image">
                        <img v-bind:src="art.image">
                    </div>
                    <div class="card-content">
                        <span class="card-title truncate">@{{art.title}}</span>
                        <p class="truncate">
                            @{{art.desc}}
                        </p>
                    </div>
                    <div class="card-action">
                        <a href="#" class="blue-grey-text bold">Lire cet article..</a>
                    </div>
                </div>
            </div>
        </div>



        <h3 class="center-align" v-if="nombre_articles==0">Il n'y a aucun article à afficher</h3>

        <div class="col l12 m12 s12 center-align">
            <ul class="pagination">
                <li v-bind:class="pagination_pos > 1 ? 'waves-effect' : 'disabled'" v-on:click="pagination_pos > 1 ? pagination_pos-- : nothing()"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                <li v-for="j in parseInt(nombre_articles/16+(nombre_articles%16!=0))" v-bind:class="j==pagination_pos ? 'blue-grey lighten-2' : ''" v-on:click="pagination_pos=j"><a href="#!">@{{j}}</a></li>
                <li v-bind:class="pagination_pos < nombre_articles/16 ? 'waves-effect' : 'disabled'" v-on:click="pagination_pos < nombre_articles/16 ? pagination_pos++ : nothing()"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
            </ul>
        </div>


    </div>

</div>
