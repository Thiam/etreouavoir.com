<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{

    public function getArticles()
    {
        return Post::all()->where('category_id', $this->id);
    }

    public static function getAllForJson()
    {
        $categories = Categorie::all()->sortByDesc('name');
        $result = collect();
        $i =  1;

        foreach($categories as $categorie)
        {
            if($categorie->slug != 'galerie' && $categorie->getArticles()->count() > 0)
                $result->push([
                    'id'    => $categorie->id,
                    'name'  => $categorie->name,
                    'slug'  => $categorie->slug
                ]);
        }

        return $result;
    }

}
