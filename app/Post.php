<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{

    public static function smartFind($id)
    {
        return Post::find($id) ?: (Post::all()->where('slug', $id)->isNotEmpty() ? Post::all()->where('slug', $id)->first() : null);
    }

    public static function getAllForJson()
    {
        $posts = Post::all()->sortByDesc('created_at');
        $result = collect();
        $i =  1;

        foreach($posts as $post)
        {
            if(!Categorie::find($post->category_id) || Categorie::find($post->category_id)->slug!='galerie')
                $result->push([
                    'id'    => $post->id,
                    'title' => $post->title,
                    'desc'  => $post->excerpt,
                    'slug'  => $post->slug,
                    'image' => $post->image_url(),
                    'num'   => $i++,
                    'categ' => $post->category_id,
                ]);
        }

        return $result;
    }

    public static function getGalerieForJson()
    {
        $posts = Post::all()->sortByDesc('created_at');
        $result = collect();
        $i =  1;
        //for($j=0 ; $j<113 ; $j++)
        foreach($posts as $post)
        {
            if(Categorie::find($post->category_id) && Categorie::find($post->category_id)->slug=='galerie')
                $result->push([
                    'id'    => $post->id,
                    'title' => $post->title." $i",
                    'desc'  => $post->excerpt,
                    'slug'  => $post->slug,
                    'image' => $post->image_url(),
                    'num'   => $i++,
                ]);
        }

        return $result;
    }

    public function image_url() {
        return Storage::url($this->image);
    }

}
