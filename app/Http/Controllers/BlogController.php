<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Categorie;

class BlogController extends Controller
{

    public function articles()
    {
        $posts = Post::getAllForJson();
        $categories = Categorie::getAllForJson();
        return collect([
            'articles'      => $posts,
            'categories'    => $categories,
            'nombre'        => $posts->count()
        ]);
    }

    public function photos()
    {
        $posts = Post::getGalerieForJson();
        return collect([
            'photos' => $posts,
            'nombre' => $posts->count()
        ]);
    }

}
