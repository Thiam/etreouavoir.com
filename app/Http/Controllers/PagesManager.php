<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Page;

class PagesManager extends Controller
{

    public function accueil()
    {
        return view('content', [
            'page'              => 'accueil',
            'page_title'        => "Accueil",
            'seo_title'         => 'Être ou Avoir - Le Blog | Accueil',
            'seo_description'   => 'Être ou Avoir est un blog humaniste de Moustapha Jah Diallo',
            'seo_keywords'      => 'Etre ou Avoir, blog, humaniste, humanisme, moustapha jah diallo',
            'seo_image'         => url('https://www.etreouavoir.com/img/moustapha-diallo.jpg'),
            'seo_type'          => 'website',
            'seo_url'           => url('/accueil'),
        ]);
    }

    public function blog()
    {
        return view('content', [
            'page'              => 'blog',
            'page_title'        => "Blog",
            'seo_title'         => 'Être ou Avoir - Le Blog | Articles',
            'seo_description'   => 'Être ou Avoir est un blog humaniste de Moustapha Jah Diallo',
            'seo_keywords'      => 'Etre ou Avoir, blog, humaniste, humanisme, moustapha jah diallo',
            'seo_image'         => url('https://www.etreouavoir.com/img/moustapha-diallo.jpg'),
            'seo_type'          => 'website',
            'seo_url'           => url('/blog'),
        ]);
    }

    public function article($slug)
    {
        if(!$article = Post::smartFind($slug))
        {
            abort(404);
        }

        return view('content', [
            'page'              => 'article',
            'page_title'        => 'Blog',
            'article'           => $article,
            'seo_title'         => 'Être ou Avoir - Le Blog | '.$article->title,
            'seo_description'   => 'Être ou Avoir - '.$article->meta_description,
            'seo_keywords'      => 'Etre ou Avoir, blog, '.$article->meta_keywords,
            'seo_image'         => url($article->image_url()),
            'seo_type'          => 'article',
            'seo_url'           => url('/blog/'.$slug),
        ]);
    }

    public function galerie()
    {
        return view('content', [
            'page' => 'galerie',
            'page_title' => "Galerie",
            'seo_title'         => 'Être ou Avoir - Le Blog | Galerie',
            'seo_description'   => 'Être ou Avoir est un blog humaniste de Moustapha Jah Diallo',
            'seo_keywords'      => 'Etre ou Avoir, blog, humaniste, humanisme, moustapha jah diallo',
            'seo_image'         => url('https://www.etreouavoir.com/img/moustapha-diallo.jpg'),
            'seo_type'          => 'website',
            'seo_url'           => url('/galerie'),
        ]);
    }

    public function apropos()
    {
        if(Page::all()->where('slug', 'a-propos')->isEmpty())
        {
            abort(404);
        }
        $page = Page::all()->where('slug', 'a-propos')->first();

        return view('content', [
            'page'          => 'a-propos',
            'page_title'    => $page->title,
            'contenu'       => $page,
            'seo_title'         => 'Être ou Avoir - Le Blog | À propos',
            'seo_description'   => 'Être ou Avoir est un blog humaniste de Moustapha Jah Diallo',
            'seo_keywords'      => 'Etre ou Avoir, blog, humaniste, humanisme, moustapha jah diallo',
            'seo_image'         => url('https://www.etreouavoir.com/img/moustapha-diallo.jpg'),
            'seo_type'          => 'website',
            'seo_url'           => url('/a-propos'),
        ]);
    }
}
